package com.pms.sdk.api.request;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;

public class LogoutPms extends BaseRequest {

	public LogoutPms(Context context) {
		super(context);
	}

	/**
	 * get param
	 * 
	 * @return
	 */
	public JSONObject getParam () {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("mktVer", FLAG_Y);

			return jobj;
		} catch (JSONException e) {
			CLog.e(e.getMessage());
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (final APICallback apiCallback) {
		try {
			PMSUtil.setCustId(mContext, "");
			mPrefs.putString(PREF_LOGINED_CUST_ID, "");
			mPrefs.putString(PREF_MAX_USER_MSG_ID, "-1");
			mDB.deleteAll();

			apiManager.call(API_LOGOUT_PMS, getParam(), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			CLog.e(e.getMessage());
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		mPrefs.putString(PREF_LOGINED_STATE, FLAG_N);
		try {
			mPrefs.putString(PREF_NOTI_FLAG, json.getString("notiFlag"));
			mPrefs.putString(PREF_MKT_FLAG, json.getString("mktFlag"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return true;
	}
}
