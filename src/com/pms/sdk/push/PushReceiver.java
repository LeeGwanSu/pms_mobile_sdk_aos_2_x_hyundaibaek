package com.pms.sdk.push;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.text.TextUtils;
import android.widget.RemoteViews;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.pms.sdk.IPMSConsts;
import com.pms.sdk.PMS;
import com.pms.sdk.api.QueueManager;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.PushMsg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.view.BitmapLruCache;

import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.List;

/**
 * push receiver
 * 
 * @author erzisk
 * @since 2013.06.07
 */
public final class PushReceiver extends BroadcastReceiver implements IPMSConsts
{
	private static final int START_TASK_TO_FRONT = 2;

	private static final String USE = "Y";

	public static final int BADGE_UPDATE_TYPE_CANCELED = 1;
	public static final int BADGE_UPDATE_TYPE_CLICKED = 2;

	public static final String BADGE_UPDATE_ACTION = "pushreceiver.badge.update";
	public static final String BADGE_UPDATE_TYPE = "pushreceiver.badge.update.type";
	public static final String BADGE_GO_BACK_ACTION = "pushreceiver.badge.go.back";

	public static final int BADGE_BEGINNING_NUMBER = 0;

	// notification id
	public final static int NOTIFICATION_ID = 0x253470;
	private static final int NOTIFICATION_GROUP_SUMMARY_ID = 1;
	private static int sNotificationId = NOTIFICATION_GROUP_SUMMARY_ID + 1;
	private Prefs mPrefs;

	private PowerManager pm;
	private PowerManager.WakeLock wl;

	private boolean mbPushImage = false;

	private static final int DEFAULT_SHOWING_TIME = 30000;

	private final Handler mFinishHandler = new Handler();

	private Bitmap mPushImage;

	@Override
	public synchronized void onReceive (final Context context, final Intent intent)
	{
		if (intent == null || intent.getAction() == null)
		{
			if (intent != null)
				CLog.e("intent action is null, intent : " + intent.toString());
			return;
		}

		CLog.i("onReceive() -> " + intent.toString());
		mPrefs = new Prefs(context);

		String message = intent.getStringExtra(ACTION_KEY_MSG);
		if (!TextUtils.isEmpty(message))
		{
			try
			{
				JSONObject msgObj = new JSONObject(message);
				if (msgObj.has(KEY_MSG_ID)) {
					intent.putExtra(KEY_MSG_ID, msgObj.getString(KEY_MSG_ID));
				}
				if (msgObj.has(KEY_NOTI_TITLE)) {
					intent.putExtra(KEY_NOTI_TITLE, msgObj.getString(KEY_NOTI_TITLE));
				}
				if (msgObj.has(KEY_MSG_TYPE)) {
					intent.putExtra(KEY_MSG_TYPE, msgObj.getString(KEY_MSG_TYPE));
				}
				if (msgObj.has(KEY_NOTI_MSG)) {
					intent.putExtra(KEY_NOTI_MSG, msgObj.getString(KEY_NOTI_MSG));
				}
				if (msgObj.has(KEY_MSG)) {
					intent.putExtra(KEY_MSG, msgObj.getString(KEY_MSG));
				}
				if (msgObj.has(KEY_SOUND)) {
					intent.putExtra(KEY_SOUND, msgObj.getString(KEY_SOUND));
				}
				if (msgObj.has(KEY_NOTI_IMG)) {
					intent.putExtra(KEY_NOTI_IMG, msgObj.getString(KEY_NOTI_IMG));
				}
				if (msgObj.has(KEY_COLOR_FLAG)) {
					intent.putExtra(KEY_COLOR_FLAG, msgObj.getString(KEY_COLOR_FLAG));
				}
				if (msgObj.has(KEY_TITLE_COLOR)) {
					intent.putExtra(KEY_TITLE_COLOR, msgObj.getString(KEY_TITLE_COLOR));
				}
				if (msgObj.has(KEY_CONTENT_COLOR)) {
					intent.putExtra(KEY_CONTENT_COLOR, msgObj.getString(KEY_CONTENT_COLOR));
				}
				if (msgObj.has(KEY_NOT_POPUP)) {
					intent.putExtra(KEY_NOT_POPUP, msgObj.getString(KEY_NOT_POPUP));
				}
				if (msgObj.has(KEY_DATA)) {
					intent.putExtra(KEY_DATA, msgObj.getString(KEY_DATA));
				}
			} catch (Exception e) {
				CLog.e(e.getMessage());
			}
		}

		if (BADGE_UPDATE_ACTION.equals(intent.getAction()))
		{
			decreaseBadgeCount(context);

			int type = intent.getIntExtra(BADGE_UPDATE_TYPE, 0);
			switch (type)
			{
				case BADGE_UPDATE_TYPE_CLICKED:
				{
					Intent innerIntent = null;
					String receiverAction = null;
					String receiverClass = null;
					receiverAction = mPrefs.getString(IPMSConsts.PREF_NOTI_RECEIVER);
					receiverAction = receiverAction != null ? receiverAction : IPMSConsts.RECEIVER_NOTIFICATION;
					receiverClass = mPrefs.getString(IPMSConsts.PREF_NOTI_RECEIVER_CLASS);

					if (receiverClass != null) {
						try {
							Class<?> cls = Class.forName(receiverClass);
							innerIntent = new Intent(context, cls).putExtras(intent.getExtras());

							if (receiverAction != null)
								innerIntent.setAction(receiverAction);

						} catch (ClassNotFoundException e) {
							CLog.e(e.getMessage());
						}
					}

					if (innerIntent == null) {
						CLog.d("innerIntent == null");
						receiverAction = receiverAction != null ? receiverAction : IPMSConsts.RECEIVER_NOTIFICATION;
						// setting push info to intent
						innerIntent = new Intent(receiverAction).putExtras(intent.getExtras());
					}

					context.sendBroadcast(innerIntent);

					break;
				}
			}
		}
		else if (BADGE_GO_BACK_ACTION.equals(intent.getAction()))
		{
			sendGoBackNotification(context);
		}
		else
		{
			// receive push message
			if (ACTION_MQTT_MSG.equals(intent.getAction()))
			{
				// private server
				CLog.i("onReceive:receive from private server");

			}
			else if (ACTION_RECEIVE.equals(intent.getAction()))
			{
				// gcm
				CLog.i("onReceive:receive from FCM(GCM)");
			}

			onPushMessage(context, intent);
		}
	}

	private synchronized void onPushMessage (final Context context, final Intent intent) {
		String firstTime = PMSUtil.getFirstTime(context);
		String secondTime = PMSUtil.getSecondTime(context);
		Boolean pushCancel = true;

		CLog.i("Allowed Time Flag : " + PMSUtil.getAllowedTime(context));

		if (PMSUtil.getAllowedTime(context)) {
			if ((StringUtil.isEmpty(firstTime) == false) && (StringUtil.isEmpty(secondTime) == false)) {
				pushCancel = DateUtil.getCompareDate(firstTime, secondTime);
			}
		}

		CLog.i("Auto Push Cancel : " + pushCancel);
		if (pushCancel) {
			if (isImagePush(intent.getExtras())) {
				try {
					// image push
					QueueManager queueManager = QueueManager.getInstance();
					RequestQueue queue = queueManager.getRequestQueue();
					queue.getCache().clear();
					ImageLoader imageLoader = new ImageLoader(queue, new BitmapLruCache());
					imageLoader.get(intent.getStringExtra(KEY_NOTI_IMG), new ImageListener() {
						@Override
						public void onResponse (ImageContainer response, boolean isImmediate) {
							if (response == null) {
								CLog.e("response is null");
								return;
							}
							if (response.getBitmap() == null) {
								CLog.e("bitmap is null");
								return;
							}

							mbPushImage = true;
							mPushImage = response.getBitmap();
							CLog.i("imageWidth:" + mPushImage.getWidth());
							onMessage(context, intent);
						}

						@Override
						public void onErrorResponse (VolleyError error) {
							CLog.e("onErrorResponse:" + error.getMessage());
							mbPushImage = false;
							// wrong img url (or exception)
							onMessage(context, intent);
						}
					});
				} catch (Exception e) {
					e.printStackTrace();
					onMessage(context, intent);
				}
			} else {
				// default push
				onMessage(context, intent);
			}
		}
	}

	/**
	 * on message (gcm, private msg receiver)
	 * 
	 * @param context
	 * @param intent
	 */
	private synchronized void onMessage (final Context context, Intent intent) {

		final Bundle extras = intent.getExtras();

		PMS pms = PMS.getInstance(context);

		PushMsg pushMsg = new PushMsg(extras);

		if (StringUtil.isEmpty(pushMsg.msgId) || StringUtil.isEmpty(pushMsg.notiTitle) || StringUtil.isEmpty(pushMsg.notiMsg)
				|| StringUtil.isEmpty(pushMsg.msgType)) {
			CLog.i("msgId or notiTitle or notiMsg or msgType is null");

//			Intent sendIntent = new Intent(context, RestartReceiver.class);
//			sendIntent.setAction(ACTION_FORCE_START);
//			context.sendBroadcast(sendIntent);
			return;
		}

		CLog.i(pushMsg + "");

		PMSDB db = PMSDB.getInstance(context);

		// check already exist msg
		Msg existMsg = db.selectMsgWhereMsgId(pushMsg.msgId);
		if (existMsg != null && existMsg.msgId.equals(pushMsg.msgId)) {
			CLog.i("already exist msg");
			return;
		}

		// insert (temp) new msg
		Msg newMsg = new Msg();
		newMsg.readYn = Msg.READ_N;
		newMsg.msgGrpCd = "999999";
		newMsg.expireDate = "0";
		newMsg.msgId = pushMsg.msgId;

		db.insertMsg(newMsg);

		// refresh list and badge
		Intent intentPush = null;

		String receiverClass = null;
//        receiverClass = ProPertiesFileUtil.getString(context, PRO_RECEIVER_CLASS);
		receiverClass = mPrefs.getString(IPMSConsts.PREF_PUSH_RECEIVER_CLASS);
		if (receiverClass != null) {
			try {
				Class<?> cls = Class.forName(receiverClass);
				intentPush = new Intent(context, cls).putExtras(extras);
				intentPush.setAction(RECEIVER_PUSH);
			} catch (ClassNotFoundException e) {
				CLog.e(e.getMessage());
			}
		}
		if (intentPush == null) {
			intentPush = new Intent(RECEIVER_PUSH).putExtras(extras);
		}

		if (intentPush != null) {
			context.sendBroadcast(intentPush);
		}

		// show noti
		// ** 수정 ** 기존 코드는 PREF_MSG_FLAG로 되어 있어 NOTI_FLAG로 변경 함
		CLog.i("NOTI FLAG : " + mPrefs.getString(PREF_NOTI_FLAG));

		if (USE.equals(mPrefs.getString(PREF_NOTI_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG))) {

			// check push flag
			if (USE.equals(mPrefs.getString(IPMSConsts.PREF_SCREEN_WAKEUP_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG))) {
				// screen on
				pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
				wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "myapp:tag");
				if (!pm.isScreenOn()) {
					wl.acquire();
					mFinishHandler.postDelayed(finishRunnable, DEFAULT_SHOWING_TIME);
				}
			}
			CLog.i("version code :" + Build.VERSION.SDK_INT);

			// execute push noti listener
			if (pms.getOnReceivePushListener() != null) {
				if (pms.getOnReceivePushListener().onReceive(context, extras)) {
					showNotification(context, extras);
				}
			} else {
				showNotification(context, extras);
			}

			CLog.i("ALERT FLAG : " + mPrefs.getString(PREF_ALERT_FLAG));
			CLog.i("NOT POPUP FLAG : " + pushMsg.notPopup);

			if ((USE.equals(pushMsg.notPopup) == false) && (USE.equals(mPrefs.getString(PREF_ALERT_FLAG)))) {
				showPopup(context, extras);
			}
		}
	}

	/**
	 * show notification
	 * 
	 * @param context
	 * @param extras
	 */
	private synchronized void showNotification (Context context, Bundle extras) {
		CLog.i("showNotification");
		CLog.i("Push Image State ->" + mbPushImage);
		if (mbPushImage) {
			showNotificationImageStyle(context, extras);
		} else {
			showNotificationTextStyle(context, extras);
		}
	}

	/**
	 * show notification text style
	 * 
	 * @param context
	 * @param extras
	 */
	private synchronized void showNotificationTextStyle (Context context, Bundle extras) {
		CLog.i("showNotificationTextStyle");
		// push info
		PushMsg pushMsg = new PushMsg(extras);

		int notificationId = getNotificationId();

		// notification
		int iconId = PMSUtil.getIconId(context);
		int largeIconId = PMSUtil.getLargeIconId(context);

		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		NotificationCompat.Builder builder;

		String strNotiChannel = DataKeyUtil.getDBKey(context, DB_NOTI_CHANNEL_ID);
		if (StringUtil.isEmpty(strNotiChannel)) {
			strNotiChannel = "0";
		}

		// Notification channel added
		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1 && PMSUtil.getTargetVersion(context) > Build.VERSION_CODES.N_MR1) {
			strNotiChannel = createNotiChannel(context, notificationManager, strNotiChannel);
			builder = new NotificationCompat.Builder(context, strNotiChannel);
			builder.setNumber(0);
		}
		else {
			builder = new NotificationCompat.Builder(context);
			builder.setNumber(PMSDB.getInstance(context).selectNewMsgCnt());
		}

		// setting ring mode
		int noti_mode = ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).getRingerMode();

		switch (noti_mode) {
			case AudioManager.RINGER_MODE_NORMAL:
				if (FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
					try
					{
						int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
						CLog.d("notiSound : "+notiSound);
						if (notiSound > 0) {
							Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
							builder.setSound(uri);
						} else {
							Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
							builder.setSound(uri);
						}
					}catch (Exception e)
					{
						CLog.e(e.getMessage());
					}
				}
				break;

			case AudioManager.RINGER_MODE_VIBRATE:
				if (FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
					builder.setDefaults(Notification.DEFAULT_VIBRATE);
				}
				break;

			case AudioManager.RINGER_MODE_SILENT:
				break;
		}

		builder.setContentIntent(makePendingIntent(context, extras, BADGE_UPDATE_TYPE_CLICKED, notificationId));
		builder.setDeleteIntent(clickListenerPendingIntent(context, extras, BADGE_UPDATE_TYPE_CANCELED, notificationId));
		builder.setAutoCancel(true);
		String contentText = PMSUtil.getBigNotiContextMsg(context);
		if(TextUtils.isEmpty(contentText)) {
			contentText = pushMsg.notiMsg;
		}
		builder.setContentText(contentText);
		builder.setContentTitle(pushMsg.notiTitle);
		builder.setTicker(pushMsg.notiMsg);
		// setting lights color
		builder.setLights(Notification.FLAG_SHOW_LIGHTS, 1000, 2000);

		// set small icon
		CLog.i("small icon :" + iconId);
		if (iconId > 0) {
			builder.setSmallIcon(iconId);
		}

		// set large icon
		CLog.i("large icon :" + largeIconId);
		if (largeIconId > 0) {
			builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIconId));
		}

		int SDK_INT = Build.VERSION.SDK_INT;
		if (PMSUtil.getIsCustNoti(context) && (FLAG_Y.equals(pushMsg.colorFlag)) && (SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)) {
			RemoteViews cutmNoti = new RemoteViews(context.getApplicationContext().getPackageName(), PMSUtil.getNotiResId(context));
			builder.setContent(cutmNoti);
			cutmNoti.setTextViewText(PMSUtil.getNotiTitleResId(context), pushMsg.notiTitle);
			cutmNoti.setTextColor(PMSUtil.getNotiTitleResId(context), Color.parseColor(pushMsg.titleColor));
			cutmNoti.setTextViewText(PMSUtil.getNotiContenResId(context), pushMsg.notiMsg);
			cutmNoti.setTextColor(PMSUtil.getNotiContenResId(context), Color.parseColor(pushMsg.contentColor));
		}

		/**
		 * 2021.06.21 현대백화점 요청에 의해 BigTextStyle 적용
		 */
		NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle(builder);
		builder.setContentTitle(pushMsg.notiTitle);
		bigTextStyle.setBigContentTitle(pushMsg.notiTitle);
		bigTextStyle.bigText(pushMsg.notiMsg);
		builder.setStyle(bigTextStyle);

		// show notification
//		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

		increaseBadgeCount(context);
		notificationManager.notify(notificationId, builder.build());
	}

	/**
	 * show notification image style
	 * 
	 * @param context
	 * @param extras
	 */
	@SuppressLint("NewApi")
	private synchronized void showNotificationImageStyle (Context context, Bundle extras) {
		CLog.i("showNotificationImageStyle");
		// push info
		PushMsg pushMsg = new PushMsg(extras);
		int notificationId = getNotificationId();

		// notification
		int iconId = PMSUtil.getIconId(context);
		int largeIconId = PMSUtil.getLargeIconId(context);

		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification.Builder builder;

		String strNotiChannel = DataKeyUtil.getDBKey(context, DB_NOTI_CHANNEL_ID);
		if (StringUtil.isEmpty(strNotiChannel)) {
			strNotiChannel = "0";
		}

		// Notification channel added
		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1 && PMSUtil.getTargetVersion(context) > Build.VERSION_CODES.N_MR1) {
			strNotiChannel = createNotiChannel(context, notificationManager, strNotiChannel);
			builder = new Notification.Builder(context, strNotiChannel);
			builder.setNumber(0);
		}
		else {
			builder = new Notification.Builder(context);
			builder.setNumber(PMSDB.getInstance(context).selectNewMsgCnt());

		}

		// setting ring mode
		int noti_mode = ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).getRingerMode();

		switch (noti_mode) {
			case AudioManager.RINGER_MODE_NORMAL:
				if (FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
					try
					{
						int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
						CLog.d("notiSound : "+notiSound);
						if (notiSound > 0) {
							Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
							builder.setSound(uri);
						} else {
							Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
							builder.setSound(uri);
						}
					}catch (Exception e)
					{
						CLog.e(e.getMessage());
					}
				}
				break;

			case AudioManager.RINGER_MODE_VIBRATE:
				if (FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
					builder.setDefaults(Notification.DEFAULT_VIBRATE);
				}
				break;

			case AudioManager.RINGER_MODE_SILENT:
				break;
		}

		builder.setContentIntent(makePendingIntent(context, extras, BADGE_UPDATE_TYPE_CLICKED, notificationId));
		builder.setDeleteIntent(clickListenerPendingIntent(context, extras, BADGE_UPDATE_TYPE_CANCELED, notificationId));
		builder.setAutoCancel(true);
		String contentText = PMSUtil.getBigNotiContextMsg(context);
		if(TextUtils.isEmpty(contentText)) {
			contentText = pushMsg.notiMsg;
		}
		builder.setContentText(contentText);
		builder.setContentTitle(pushMsg.notiTitle);
		builder.setTicker(pushMsg.notiMsg);
		// setting lights color
		builder.setLights(0xFF00FF00, 1000, 2000);

		// set small icon
		CLog.i("small icon :" + iconId);
		if (iconId > 0) {
			builder.setSmallIcon(iconId);
		}

		// set large icon
		CLog.i("large icon :" + largeIconId);
		if (largeIconId > 0) {
			builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIconId));
		}

		if (mPushImage == null) {
			CLog.e("mPushImage is null");
		}

		// show notification
//		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification.BigPictureStyle(builder).bigPicture(mPushImage).setBigContentTitle(pushMsg.notiTitle).setSummaryText(pushMsg.notiMsg).build();

		increaseBadgeCount(context);
		notificationManager.notify(notificationId, notification);
	}

	private PendingIntent makePendingIntent (Context context, Bundle extras, int type, int notificationId) {
		/***
		 * targetSDK 31 이전 버전의 경우 PushReceiver로 다시 전송해서 등록된 Listener에 메세지를 전달 하는 로직.
		 * targetSDK 31의 경우 알림 트램펄린 제한으로 인하여 PendingIntent에 이동할 Activity 선언
		 * 설정 값이 없다면 기존 Listener에 전달하는 기능 그대로 활용
		 */
		String activityAction = PMSUtil.getNotificationClickActivityAction(context);
		String activityClass = PMSUtil.getNotificationClickActivityClass(context);
		if(TextUtils.isEmpty(activityClass)){
			return clickListenerPendingIntent(context, extras, type, notificationId);
		}

		Intent intent = null;
		try	{
			Class<?> cls = Class.forName(activityClass);
			intent = new Intent(context, cls);

		} catch (ClassNotFoundException e) {
			CLog.i("cannot found ("+(activityClass)+")");
		}

		if(intent == null){
			return null;
		}

		intent.putExtras(extras);
		intent.setAction(activityAction);

		if(PMSUtil.getNotificationClickActivityUseBackStack(context)) {
			TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
			stackBuilder.addNextIntentWithParentStack(intent);
			if (PMSUtil.getTargetVersion(context) >= 31) {
				return stackBuilder.getPendingIntent(notificationId + type,
													 PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
			} else {
				return stackBuilder.getPendingIntent(notificationId + type,
													 PendingIntent.FLAG_UPDATE_CURRENT);
			}
		} else {
			if (PMSUtil.getTargetVersion(context) >= 31) {
				return PendingIntent.getActivity(context, notificationId + type, intent,
												 PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
			} else {
				return PendingIntent.getActivity(context, notificationId + type, intent,
												 PendingIntent.FLAG_UPDATE_CURRENT);
			}
		}
	}

	private PendingIntent clickListenerPendingIntent (Context context, Bundle extras, int type, int notificationId) {
		Intent intent = new Intent(context, PushReceiver.class);
		intent.setAction(BADGE_UPDATE_ACTION);
		intent.putExtras(extras);
		intent.putExtra(BADGE_UPDATE_TYPE, type);

		if (PMSUtil.getTargetVersion(context) >= 31) {
			return PendingIntent.getBroadcast(context, notificationId + type, intent,
											  PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
		} else {
			return PendingIntent.getBroadcast(context, notificationId + type, intent,
											  PendingIntent.FLAG_UPDATE_CURRENT);
		}
	}

//	/**
//	 * make pending intent
//	 *
//	 * @param context
//	 * @param extras
//	 * @return
//	 */
//	private PendingIntent makePendingIntent (Context context, Bundle extras) {
//		// notification
//		Intent innerIntent = null;
////		String receiverClass = ProPertiesFileUtil.getString(context, PRO_NOTI_RECEIVER);
//		String receiverClass = mPrefs.getString(PREF_NOTI_RECEIVER_CLASS);
//		String receiverAction = mPrefs.getString(PREF_NOTI_RECEIVER);
//		CLog.i("makePendingIntent receiverClass : " + receiverClass);
//		CLog.i("makePendingIntent receiverAction : " + receiverAction);
//		if (receiverClass != null) {
//			try {
//				Class<?> cls = Class.forName(receiverClass);
//				innerIntent = new Intent(context, cls).putExtras(extras);
//				if (receiverAction != null)
//					innerIntent.setAction(receiverAction);
//			} catch (ClassNotFoundException e) {
//				CLog.e(e.getMessage());
//			}
//		}
//
//		if (innerIntent == null) {
//			CLog.d("innerIntent == null");
//			receiverAction = receiverAction != null ? receiverAction : "com.pms.sdk.notification";
//			// setting push info to intent
//			innerIntent = new Intent(receiverAction).putExtras(extras);
//		}
//
//		if (innerIntent == null) return null;
//
//		return PendingIntent.getBroadcast(context, 0, innerIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//	}

	/**
	 * show popup (activity)
	 * 
	 * @param context
	 * @param extras
	 */
	@SuppressLint("DefaultLocale")
	private synchronized void showPopup (Context context, Bundle extras) {

		PushMsg pushMsg = new PushMsg(extras);

		if (PMSUtil.getNotiOrPopup(context) && pushMsg.msgType.equals("T")) {
			Toast.makeText(context, pushMsg.notiMsg, Toast.LENGTH_SHORT).show();
		} else {
			Class<?> pushPopupActivity;

			String pushPopupActivityName = mPrefs.getString(PREF_PUSH_POPUP_ACTIVITY);

			if (StringUtil.isEmpty(pushPopupActivityName)) {
				pushPopupActivityName = DEFAULT_PUSH_POPUP_ACTIVITY;
			}

			try {
				pushPopupActivity = Class.forName(pushPopupActivityName);
			} catch (ClassNotFoundException e) {
				CLog.e(e.getMessage());
				pushPopupActivity = PushPopupActivity.class;
			}
			CLog.i("pushPopupActivity :" + pushPopupActivityName);

			Intent pushIntent = new Intent(context, pushPopupActivity);
			pushIntent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION
					| Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
			pushIntent.putExtras(extras);

			if (isOtherApp(context)) {
				context.startActivity(pushIntent);
			}
		}
	}

	@SuppressWarnings({ "deprecation", "static-access" })
	@SuppressLint("DefaultLocale")
	private boolean isOtherApp (Context context) {
		ActivityManager am = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
		String topActivity = "";

		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
			RunningAppProcessInfo currentInfo = null;
			Field field = null;
			try {
				field = RunningAppProcessInfo.class.getDeclaredField("processState");
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
			}

			List<RunningAppProcessInfo> appList = am.getRunningAppProcesses();
			for (RunningAppProcessInfo app : appList) {
				if (app.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
					Integer state = null;
					try {
						state = field.getInt(app);
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					}
					if (state != null && state == START_TASK_TO_FRONT) {
						currentInfo = app;
						break;
					}
				}
			}

			if (currentInfo == null)
			{
				return false;
			}

			topActivity = currentInfo.processName;
		} else {
			List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(10);
			topActivity = taskInfo.get(0).topActivity.getPackageName();
		}

		CLog.i("TOP Activity : " + topActivity);
		if (topActivity.equals(context.getPackageName())) {
			return true;
		}
		return false;
	}

	/**
	 * is image push
	 * 
	 * @param extras
	 * @return
	 */
	private boolean isImagePush (Bundle extras) {
		try {
			if (!PhoneState.isNotificationNewStyle()) {
				throw new Exception("wrong os version");
			}
			String notiImg = extras.getString(KEY_NOTI_IMG);
			CLog.i("notiImg:" + notiImg);
			if (notiImg == null || "".equals(notiImg)) {
				return false;
			}
			return true;
		} catch (Exception e) {
			CLog.e("isImagePush:" + e.getMessage());
			return false;
		}
	}

	/**
	 * finish runnable
	 */
	private final Runnable finishRunnable = new Runnable() {
		@Override
		public void run () {
			if (wl != null && wl.isHeld()) {
				wl.release();
			}
		};
	};

	@TargetApi(Build.VERSION_CODES.O)
	private String createNotiChannel(Context context, NotificationManager notificationManager, String strNotiChannel)
	{
		NotificationChannel notiChannel = notificationManager.getNotificationChannel(strNotiChannel);
		boolean isShowBadge = false;
		boolean isPlaySound;
		boolean isPlaySoundChanged = false;
		boolean isEnableVibe;
		boolean isShowBadgeOnChannel;
		boolean isPlaySoundOnChannel;
		boolean isEnableVibeOnChannel;
		boolean isAlarmVolumeZero;

		try
		{
			if(IPMSConsts.FLAG_Y.equals((String) context.getPackageManager().getApplicationInfo(context.getPackageName(),
					PackageManager.GET_META_DATA).metaData.get(IPMSConsts.META_DATA_NOTI_O_BADGE)))
			{
				isShowBadge = true;
			}
			else
			{
				isShowBadge = false;
			}
		}
		catch (PackageManager.NameNotFoundException e)
		{
			CLog.e(e.getMessage());
		}

		if(IPMSConsts.FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG)))
		{
			isPlaySound = true;
		}
		else
		{
			isPlaySound = false;
		}
		if(IPMSConsts.FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)))
		{
			isEnableVibe = true;
		}
		else
		{
			isEnableVibe = false;
		}
		int noti_mode = ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).getRingerMode();
		try
		{
			CLog.d("AppSetting isShowBadge "+ context.getPackageManager().getApplicationInfo(context.getPackageName(),
					PackageManager.GET_META_DATA).metaData.get(IPMSConsts.META_DATA_NOTI_O_BADGE)+
					" isPlaySound "+mPrefs.getString(PREF_RING_FLAG)+
					" isEnableVibe "+mPrefs.getString(PREF_VIBE_FLAG));
		}
		catch (PackageManager.NameNotFoundException e)
		{
			CLog.e(e.getMessage());
		}

		if (notiChannel == null)
		{    //if notichannel is not initialized
			CLog.d("notification initialized");
			notiChannel = new NotificationChannel(strNotiChannel, PMSUtil.getApplicationName(context), NotificationManager.IMPORTANCE_HIGH);
			notiChannel.setShowBadge(isShowBadge);
			notiChannel.enableVibration(isEnableVibe);
			notiChannel.setLightColor(Notification.FLAG_SHOW_LIGHTS);
			notiChannel.setImportance(NotificationManager.IMPORTANCE_HIGH);
			notiChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
			if (isEnableVibe)
			{
				notiChannel.setVibrationPattern(new long[]{1000, 1000});
			}

			if (isPlaySound)
			{
				Uri uri;
				try
				{
					int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
					if (notiSound > 0)
					{
						uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
						CLog.d("notiSound " + notiSound + " uri " + uri.toString());
					} else
					{
						uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
					}
				}
				catch (Exception e)
				{
					uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
					CLog.e(e.getMessage());
				}
				AudioAttributes audioAttributes = new AudioAttributes.Builder()
						.setUsage(AudioAttributes.USAGE_NOTIFICATION)
						.build();
				notiChannel.setSound(uri, audioAttributes);
				CLog.d("setChannelSound ring with initialize notichannel");
			} else
			{
				notiChannel.setSound(null, null);
				CLog.d("setChannelSound muted with initialize notichannel");
			}
			notificationManager.createNotificationChannel(notiChannel);
			return strNotiChannel;
		} else
		{
			CLog.d("notification is exist");
			if (notiChannel.canShowBadge())
			{
				isShowBadgeOnChannel = true;
			} else
			{
				isShowBadgeOnChannel = false;
			}
			if (notiChannel.getSound() != null)
			{
				isPlaySoundOnChannel = true;
				int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
				Uri uri;
				if (notiSound > 0)
				{
					uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
				}
				else
				{
					uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
				}
				CLog.d("channel : "+notiChannel.getSound().toString()+" set : "+uri.toString());
				if (!notiChannel.getSound().toString().equals(uri.toString()))
				{
					isPlaySoundChanged = true;
				}
				else
				{
					isPlaySoundChanged = false;
				}
			} else
			{
				isPlaySoundOnChannel = false;
			}
			if (notiChannel.shouldVibrate())
			{
				isEnableVibeOnChannel = true;
			} else
			{
				isEnableVibeOnChannel = false;
			}
			String isShowBadgeOnChannelString = isShowBadgeOnChannel ? "Y" : "N";
			String isPlaySoundOnChannelString = isPlaySoundOnChannel ? "Y" : "N";
			String isEnableVibeOnChannelString = isEnableVibeOnChannel ? "Y" : "N";
			String isPlaySoundChangedString = isPlaySoundChanged ? "Y" : "N";
			CLog.d("ChannelSetting isShowBadge " + isShowBadgeOnChannelString +
					" isPlaySound " + isPlaySoundOnChannelString +
					" isEnableVibe " + isEnableVibeOnChannelString +
					" isPlaySoundChanged " + isPlaySoundChangedString);

			//if notichannel is exist -> check setting from channel with app setting
			if ((isShowBadge != isShowBadgeOnChannel) || (isPlaySound != isPlaySoundOnChannel) || (isEnableVibe != isEnableVibeOnChannel) || isPlaySoundChanged)
			{
				CLog.d("notification setting is not matched");
				notificationManager.deleteNotificationChannel(strNotiChannel);
				//is not matched
				int currentChannelInteger = 0;
				try
				{
					currentChannelInteger = Integer.parseInt(strNotiChannel);
				}
				catch (Exception e)
				{
					CLog.d("currentChannel parsing " + e.getMessage());
				}
				String newChannelString = ++currentChannelInteger + "";
				CLog.d("newchannelString : " + newChannelString);
				NotificationChannel newChannel = new NotificationChannel(newChannelString, PMSUtil.getApplicationName(context), NotificationManager.IMPORTANCE_HIGH);
				newChannel.setShowBadge(isShowBadge);
				newChannel.enableVibration(isEnableVibe);
				newChannel.setLightColor(Notification.FLAG_SHOW_LIGHTS);
				newChannel.setImportance(NotificationManager.IMPORTANCE_HIGH);
				newChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
				if (isEnableVibe)
				{
					notiChannel.setVibrationPattern(new long[]{1000, 1000});
				}

				if (isPlaySound)
				{
					Uri uri;
					try
					{
						int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
						if (notiSound > 0)
						{
							uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
							CLog.d("notiSound " + notiSound + " uri " + uri.toString());
						} else
						{
							throw new Exception("default ringtone is set");
						}
					}
					catch (Exception e)
					{
						uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
						CLog.e(e.getMessage());
					}
					AudioAttributes audioAttributes = new AudioAttributes.Builder()
							.setUsage(AudioAttributes.USAGE_NOTIFICATION)
							.build();
					newChannel.setSound(uri, audioAttributes);
					CLog.d("setChannelSound ring with initialize notichannel");
				} else
				{
					newChannel.setSound(null, null);
					CLog.d("setChannelSound muted with initialize notichannel");
				}
				notificationManager.createNotificationChannel(newChannel);

				DataKeyUtil.setDBKey(context, DB_NOTI_CHANNEL_ID, newChannelString);
				return newChannelString;
			} else
			{
				//is matched
				return strNotiChannel;
			}
		}
	}
	public int getNewNotificationId () {
		int notificationId = sNotificationId++;

		// Unlikely in the sample, but the int will overflow if used enough so we skip the summary
		// ID. Most apps will prefer a more deterministic way of identifying an ID such as hashing
		// the content of the notification.
		if (notificationId == NOTIFICATION_GROUP_SUMMARY_ID) {
			notificationId = sNotificationId++;
		}
		return notificationId;
	}

	private void increaseBadgeCount(Context context)
	{
		updateNotificationBadge(context, 1);
	}

	private void decreaseBadgeCount(Context context)
	{
		updateNotificationBadge(context, BADGE_BEGINNING_NUMBER);
	}

	private void updateNotificationBadge(Context context, int count)
	{
//		if(PMSUtil.isBadgeUse(context))
//		{
//			Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
//			if(launchIntent != null)
//			{
//				ComponentName componentName = launchIntent.getComponent();
//				Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
//				intent.putExtra("badge_count_package_name", componentName.getPackageName());
//				intent.putExtra("badge_count_class_name", componentName.getClassName());
//				intent.putExtra("badge_count", count);
//
//				CLog.d("badge : "+count+" package : "+componentName.getPackageName()+" class : "+componentName.getClassName());
//				context.sendBroadcast(intent);
//			}
//		}
	}

	public void sendGoBackNotification(Context context)
	{
//		NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//
//		manager.cancelAll();
		updateNotificationBadge(context, BADGE_BEGINNING_NUMBER);
	}

	public int getNotificationId () {
		int notificationId = (int)(System.currentTimeMillis() % Integer.MAX_VALUE);
		return notificationId;
	}
}
