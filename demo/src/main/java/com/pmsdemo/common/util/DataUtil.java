package com.pmsdemo.common.util;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.pms.sdk.common.util.StringUtil;
import com.pmsdemo.R;
import com.pmsdemo.common.IPMSDEMOConts;

public class DataUtil implements IPMSDEMOConts {

	private static LinearLayout subLayout2;

	private static TextView mtvEventbtn;

	public static void loginDialog (Context con, Boolean state, DialogInterface.OnClickListener click, RadioGroup chcek, EditText... edit) {
		AlertDialog.Builder alert = new AlertDialog.Builder(con);

		alert.setTitle(con.getString(R.string.btn_login));
		alert.setMessage(con.getString(R.string.dialog_content_2));

		ScrollView scrollView = new ScrollView(con);

		LinearLayout mainLayout = new LinearLayout(con);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		mainLayout.setPadding(50, 50, 50, 50);

		LinearLayout subLayout1 = new LinearLayout(con);
		subLayout1.setOrientation(LinearLayout.VERTICAL);

		subLayout2 = null;
		subLayout2 = new LinearLayout(con);
		subLayout2.setOrientation(LinearLayout.VERTICAL);
		subLayout2.setVisibility(View.GONE);

		mtvEventbtn = null;
		mtvEventbtn = new TextView(con);
		mtvEventbtn.setText(con.getString(R.string.btn_add));
		mtvEventbtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick (View v) {
				subLayout2.setVisibility(View.VISIBLE);
				mtvEventbtn.setVisibility(View.GONE);
			}
		});

		if (state) {
			subLayout1.removeAllViews();
			subLayout2.removeAllViews();
			mainLayout.removeAllViews();
		}

		subLayout1.addView(edit[0]);
		subLayout1.addView(mtvEventbtn);

		for (int i = 1; i < edit.length; i++) {
			subLayout2.addView(edit[i]);
		}
		subLayout2.addView(chcek);

		mainLayout.addView(subLayout1);
		mainLayout.addView(subLayout2);

		scrollView.addView(mainLayout);

		alert.setView(scrollView);

		alert.setPositiveButton(con.getString(R.string.btn_unlogin), click);
		alert.setNegativeButton(con.getString(R.string.btn_login), click);

		alert.show();
	}

	public static Boolean checkParameter (JSONObject json) {
		try {
			if (StringUtil.isEmpty(json.getString("custName")) && StringUtil.isEmpty(json.getString("phoneNumber"))
					&& StringUtil.isEmpty(json.getString("birthday")) && StringUtil.isEmpty(json.getString("location1"))
					&& StringUtil.isEmpty(json.getString("location2")) && StringUtil.isEmpty(json.getString("gender"))
					&& StringUtil.isEmpty(json.getString("data1")) && StringUtil.isEmpty(json.getString("data2"))
					&& StringUtil.isEmpty(json.getString("data3"))) {
				return false;
			} else {
				return true;
			}
		} catch (JSONException e) {
			e.printStackTrace();
			return false;
		}
	}
}
