package com.pmsdemo.common;

public interface IPMSDEMOConts {

	public static final String PREF_APP_FIRST = "app_first";
	public static final String PREF_PHONENUMBER = "pref_phonenumber";

	public static final String RECEIVER_LIST_REQUERY = "receiver_list_requery";

	public static final String PAGE_ROW = "100";

	public static final int DB_PAGE = 1;
	public static final int DB_ROW = 30;

	public static final int SETTING = 1;
	public static final int BTN_LOGIN = 2;
	public static final int BTN_EDIT = 3;
	public static final int BTN_ALL_SELECT = 4;
	public static final int BTN_DEL = 5;
	public static final int BTN_CANCEL = 6;
	public static final int BTN_SETCONFIG = 7;

	public static final Boolean IS_PHONE_NUMBER = false;

}
